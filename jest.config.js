module.exports = {
  'moduleFileExtensions': [
    'js',
    'json',
    'vue',
  ],
  'transform': {
    '.*\\.(vue)$': 'vue-jest',
    '^.+\\.js$': '<rootDir>/node_modules/babel-jest',
  },
  'collectCoverage': true,
  'collectCoverageFrom': [
    'src/**/*.{js,vue}',
    '!src/index.js',
    '!src/sw/*.js',
  ],
  'coverageReporters': [
    'html',
    'text-summary',
  ],
};
