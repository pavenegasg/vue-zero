import Vue from 'vue/dist/vue.js';
import VueRouter from 'vue-router';
import App from './App.vue';

const Dumb = () => import(/* webpackChunkName: "dumb" */ './dumb.vue');

Vue.use(VueRouter);

const routes = [
  {path: '/', component: App},
  {
    path: '/about',
    component: Dumb,
  },
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

new Vue({
  template: `
  <div id="app">
    <a href="/about"> About </a>
    <router-view class="view"></router-view>
  </div>
  `,
  router,
}).$mount('#app');
