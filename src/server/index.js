const express = require('express');
const path = require('path');

const app = express();
const dist = path.resolve(path.join(__dirname, '../../dist'));

app.use(express.static(dist));

app.all('/*', function(req, res) {
  res.sendFile('index.html', {root: dist});
});

app.listen(4444, () => {
  console.log('Listening on 4444');
});
