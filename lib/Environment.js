/* eslint-disable */
const convict = require('convict');
const path = require('path');

class Environment {
  constructor({ schema = {} }) {
    this.environment = '';
    this.envVars = {};
    this.schema = schema;
  }

  setup({ schema, files, environment }) {
    this.setInstanceVars(files);
    this.setProcessEnvironmentVars();

    return this.getEnvironmentVars();
  }

  setInstanceVars(files) {
    const vars = this.loadFiles(files);
    const secrets = this.loadSecrets();

    const envVars = {
      ...vars,
      ...secrets,
    };

    const config = convict(this.schema);

    config.load(envVars);
    config.validate({ allowed: 'strict' });

    this.envVars = envVars;
  }

  loadFiles(filesArr) {
    const vars = filesArr.reduce((accum, file) => {
      const filePath = path.resolve(
        path.join(process.cwd(), '/settings', `/${file}`)
      );

      const fileVars = require(filePath);

      return { ...accum, ...fileVars };
    }, {});

    return vars;
  }

  loadSecrets() {
    let secrets = {};

    const secretsPath = path.resolve(
      path.join(process.cwd(), '/settings', '/secrets')
    );

    try {
      secrets = require(secretsPath);
    } catch (err) {
      console.error(
        'WARNING: .secrets file does not exists in the ./settings folder'
      );
    }

    return secrets;
  }

  setProcessEnvironmentVars() {
    const { envVars } = this;

    Object.keys(envVars).forEach((item) => {
      process.env[item] = envVars[item];
    });
  }

  getEnvironmentVars() {
    this._validateInstanceVars();

    return { ...this.envVars };
  }

  getWebpackEnvironmentVars() {
    this._validateInstanceVars();

    const vars = {};

    Object.keys(this.envVars).map((key) => {
      vars[key] = `'${this.envVars[key]}'`;
    });

    return vars;
  }

  _validateInstanceVars() {
    if (!Object.keys(this.envVars).length) {
      this.setInstanceVars();
    }
  }
}

module.exports = Environment;
