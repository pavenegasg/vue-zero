const path = require('path');
const webpack = require('webpack');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const config = require('../settings/_setup');

config.setup({
  files: ['base', process.env.NODE_ENV],
  environment: process.env.NODE_ENV,
});

const envVars = config.getWebpackEnvironmentVars();

module.exports = {
  entry: {
    'main': path.resolve(__dirname, '../src/index.js'),
  },

  optimization: {
    splitChunks: {
      chunks: 'all',
    },
  },

  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: (file) => /node_modules/.test(file) && !/\.vue\.js/.test(file),
      },
    ],
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        ...envVars,
      },
    }),

    new VueLoaderPlugin(),

    new HtmlWebpackPlugin({
      title: 'App title',
      filename: 'index.html',
      template: path.resolve(__dirname, '../src/index.html'),
      inject: true,
    }),
  ],
};
