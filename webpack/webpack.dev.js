const path = require('path');
const shared = require('./shared.webpack');

module.exports = {
  entry: shared.entry,
  optimization: shared.optimization,

  mode: 'development',

  output: {
    path: path.resolve(__dirname, '../dist'),
    chunkFilename: '[name].bundle.js',
  },

  devtool: 'inline-source-map',

  devServer: {
    host: '0.0.0.0',
    port: process.env.PORT,
    contentBase: path.resolve(__dirname, '../dist'),
    historyApiFallback: true,
    compress: true,
  },

  module: {
    rules: [
      ...shared.module.rules,
      {
        test: /\.scss$/,
        use: [
          'vue-style-loader',
          {
            loader: 'css-loader',
            options: {sourceMap: true},
          },
          {
            loader: 'sass-loader',
            options: {sourceMap: true},
          },
        ],
      },
    ],
  },

  plugins: [...shared.plugins],
};
