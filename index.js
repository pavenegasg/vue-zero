const shell = require('shelljs');

const ENV = process.env.NODE_ENV;

if (ENV === 'development') {
  shell.exec('yarn dev');
} else {
  // add pm2 shit
  shell.exec('yarn serve');
}
