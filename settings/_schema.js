module.exports = {
  NODE_ENV: {
    doc: 'App environment',
    format: ['production', 'staging', 'development', 'test'],
    default: 'development',
  },

  APP_NAME: {
    doc: 'Application name',
    format: String,
    default: 'vue-zero',
  },

  WEBPACK_DEBUG_PORT: {
    doc: 'Inspector debug port number',
    format: 'port',
    default: 5858,
  },

  WEBPACK_PORT: {
    doc: 'Webpack dev server port number',
    format: 'port',
    default: 8080,
  },
};
