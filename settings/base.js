module.exports = {
  NODE_ENV: 'development',
  APP_NAME: 'vue-zero',
  WEBPACK_PORT: process.env.WEBPACK_PORT || 8080,
  WEBPACK_DEBUG_PORT: process.env.WEBPACK_DEBUG_PORT || 5858,
};
