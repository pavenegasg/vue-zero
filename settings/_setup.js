const Config = require('../lib/Environment');
const schema = require('./_schema');

const environment = new Config({schema});

environment.setup({
  files: ['base'],
  environment: 'development',
});

module.exports = environment;
