module.exports = {
  'presets': [
    [
      '@babel/preset-env',
      {
        'targets': {
          'edge': '17',
          'firefox': '60',
          'chrome': '67',
          'safari': '11.1',
        },
        'shippedProposals': true,
      },
    ],
  ],

  'env': {
    'test': {
      'presets': [['@babel/preset-env', {'targets': {'node': 'current'}}]],
    },
  },

  'plugins': ['@babel/plugin-syntax-dynamic-import'],
};
